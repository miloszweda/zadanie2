import csv

from django.http import HttpResponse
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from django_filters import rest_framework as filters
from user.models import BaseUser
from user.serializers import UserSerializer, UserRepresentationSerializer


class UserViewSet(ModelViewSet):
    queryset = BaseUser.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('email', 'id')
    SERIALIZERS_MAP = {}

    def get_serializer_class(self):
        return self.SERIALIZERS_MAP.get(self.action, UserSerializer)

    @action(detail=False, methods=('get', ))
    def export_data(self, request, *args, **kwargs):
        users = self.queryset.all()
        response = HttpResponse()
        response['Content-Disposition'] = 'attachment; filename=users.csv'
        writer = csv.writer(response)
        writer.writerow(['email', 'fizz_buzz', 'random_number', 'allowed'])
        for user in users:
            serialized_user_object = UserRepresentationSerializer(instance=user).data
            writer.writerow(
                [serialized_user_object['email'],
                 serialized_user_object['bizz_fuzz'],
                 serialized_user_object['random_number'],
                 serialized_user_object['allowed']])
        return response



