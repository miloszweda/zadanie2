from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin


class BaseUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    birthday = models.DateField()
    random_number = models.IntegerField(null=True)

    USERNAME_FIELD = 'email'
