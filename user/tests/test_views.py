import pytest
import datetime
from rest_framework import status

from user.tests.factories import UserFactory


@pytest.mark.django_db
class TestUserViewSet:

    @pytest.mark.freeze_time('2020-09-23 00:00:00')
    def test_get_all_users(self, api_client):
        users = [UserFactory() for _ in range(10)]
        response = api_client.get('/users/')
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == len(users)

    @pytest.mark.freeze_time('2020-09-23 00:00:00')
    def test_get_user(self, api_client):
        user = UserFactory(random_number=15)
        response = api_client.get(f'/users/{user.id}/')
        assert response.status_code == status.HTTP_200_OK
        assert response.data['bizz_fuzz'] == 'BizzFuzz'
        assert response.data['email'] == user.email

    def test_create_user(self, api_client):
        payload = {'email': 'test123@email.pl', 'birthday': str(datetime.date.today()), 'password': 'hasełko123'}
        response = api_client.post('/users/', data=payload)
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data['email'] == payload['email']

    def test_update_user(self, api_client):
        user = UserFactory()
        payload = {'email': 'changed_email@email.pl'}
        response = api_client.patch(f'/users/{user.id}/', data=payload)
        assert response.status_code == status.HTTP_200_OK
        assert response.data['email'] == payload['email']

    def test_delete_user(self, api_client):
        user = UserFactory()
        response = api_client.delete(f'/users/{user.id}/')
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_get_bizz_fuzz_and_allowed(self, api_client):
        user = UserFactory(random_number=45)
        response = api_client.get(f'/users/{user.id}/')
        assert response.status_code == status.HTTP_200_OK
        assert response.data['bizz_fuzz'] == 'BizzFuzz'



