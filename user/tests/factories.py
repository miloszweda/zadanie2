import datetime
import random
import factory.django
from factory.fuzzy import FuzzyDate

from user.models import BaseUser


class UserFactory(factory.django.DjangoModelFactory):
    birthday = FuzzyDate(datetime.date(1995, 8, 14))
    random_number = factory.fuzzy.FuzzyInteger(low=0, high=100)
    email = factory.fuzzy.FuzzyText(f'email{random.randint(0,100)}{random.randint(0, 100)}@email.pl')
    class Meta:
        model = BaseUser
