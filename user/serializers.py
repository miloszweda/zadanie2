import random
import datetime

from rest_framework import serializers

from user.models import BaseUser


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = BaseUser
        fields = '__all__'

    def create(self, validated_data):
        validated_data['random_number'] = random.randint(0, 100)
        instance = super().create(validated_data)
        instance.set_password(validated_data['password'])
        instance.save()
        return instance

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)
        self.__update_password(instance, validated_data)
        return instance

    def __update_password(self, instance, validated_data):
        if 'password' in validated_data:
            instance.set_password(validated_data['password'])
            instance.save()

    def to_representation(self, instance):
        return UserRepresentationSerializer(instance=instance).data


class UserRepresentationSerializer(serializers.Serializer):
    email = serializers.EmailField(read_only=True)
    birthday = serializers.DateField(read_only=True)
    random_number = serializers.IntegerField(read_only=True)
    bizz_fuzz = serializers.SerializerMethodField()
    allowed = serializers.SerializerMethodField()

    def get_bizz_fuzz(self, instance):
        if instance.random_number % 3 == 0 and instance.random_number % 5 == 0:
            return 'BizzFuzz'
        if instance.random_number % 3 == 0:
            return 'Bizz'
        if instance.random_number % 5 == 0:
            return 'Fuzz'
        return instance.random_number

    def get_allowed(self, instance):
        return 'allowed' if (datetime.date.today().year - instance.birthday.year) > 13 else 'blocked'



